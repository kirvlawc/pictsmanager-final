const db = require("../models");
const { Role, ROLES} = db;

/**
 * Inserts initial mandatory values into the database
 */
const initializeDB = () => {
    Role.estimatedDocumentCount((err, count) => {
        if (!err && count === 0) {
            if (Array.isArray(ROLES) && ROLES.length) {
                ROLES.forEach((role) => {
                    new Role({ name: role})
                        .save((err) => {
                            if (err) {
                                console.log("Error initializing DB: ", err);
                            }

                            console.log(`Added '${role}' to roles collection`);
                        });
                });
            }
        }
    });
}

exports.initializeDB = initializeDB;
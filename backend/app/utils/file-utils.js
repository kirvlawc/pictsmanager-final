const fs = require('fs');

/**
 * Deletes a list of files asynchronously
 * @param {*} files Array of file paths to be deleted
 * @param {*} callback Callback function to be called when deletion has completed or on error
 */
const deleteFiles = (files, callback) => {
    var numFiles = files.length;

    files.forEach((filepath) => {
        if (fs.existsSync()) {
            fs.unlink(filepath, function (err) {
                numFiles--;
                if (err) {
                    callback(err);
                    return;
                } else if (numFiles <= 0) {
                    callback(null);
                }
            });
        }
    });
}


module.exports = {
    deleteFiles
};
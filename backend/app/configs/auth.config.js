module.exports = {
    secret: process.env.AUTH_SECRET_KEY,
    jwtExpiration: parseInt(process.env.TOKEN_LIFESPAN),
    jwtRefreshExpiration: parseInt(process.env.REFRESH_TOKEN_LIFESPAN)
};
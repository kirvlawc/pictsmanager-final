const multer = require('multer');
const path = require('path')
const { v4 } = require('uuid');


const imageFilter = (req, file, cb) => {
    if (
        !file.mimetype.startsWith('image') ||
        !file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)
    ) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error(req.fileValidationError), false);
    }

    cb(null, true);
};

const imageStorage = () => {
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './app/public/images/')
        },
        filename: function (req, file, cb) {
            const uniqueName = v4() + path.extname(file.originalname);
            cb(null, uniqueName);
        }
    })

    return storage;
};

module.exports = {
    imageFilter,
    imageStorage
};
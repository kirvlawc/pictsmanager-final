const { Album, Image, mongoose } = require('../models');
const { deleteFiles } = require('../utils/file-utils');


class AlbumController {

    /**
     * Creates an album from the data provided in the request body. On creation the parameter `images` is ignored.
     * Any images that need to be added to the album can be done so through a subsequent request. The `owner` parameter
     * is also ignored as it is deduced from the JWT token.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    create(req, res) {
        const newAlbum = this.parse(req.body);
        newAlbum.owner = req.userId;

        newAlbum.save((err, album) => {
            if (err) {
                console.log(err);
                return res.status(500).send({ message: "An unexpected error occured." });
            }  else {
                return res.send({ message: "Album created successfully." });
            }
        });
    }

    /**
     * Gets all the albums that the current user has access to(being the owner or a allowed viewer). Results are paginated
     * by default to prevent long response times and heavy response bodies. The page number and page size can be specified
     * through the uses of the query parameters `page` and `size`. **Note** that the page number always starts at `0`.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    getAlbums(req, res) {
        const page = req.query.page ? parseInt(req.query.page) : null;
        const size = req.query.size ? parseInt(req.query.size) : null;
        const { limit, offset } = this.getPagination(page, size);

        const condition = {
            $or: [
                { owner: req.userId },
                { viewers: req.userId }
            ]
        };

        Album.paginate(condition, { offset, limit })
            .then((data) => {
                res.send({
                    totalItems: data.totalDocs,
                    totalPages: data.totalPages,
                    currentPage: data.page - 1,
                    albums: data.docs,
                });
            }).catch((err) => {
                console.log(err);
                res.status(500).send({ message: "An unexpected error occured." });
            });

    }

    /**
     * Deletes an album that the current user is the owner of. The query paramater `imgdel` can also be specified as `true`
     * to delete images associated to this album.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    delete(req, res) {
        const albumId = req.params.id;
        const deleteAlbumImages = (req.query.imgdel && req.query.imgdel === "true") ? true : false;

        Album.findOneAndDelete({ _id: albumId, owner: req.userId })
            .populate("images", "filename")
            .exec((err, album) => {
                if (err) {
                    if (err instanceof mongoose.Error.CastError) {
                        return res.status(500).send({ message: "Invalid input." });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occurred." });
                    }
                }

                if (!album) {
                    return res.status(404).send({ message: `Cannot delete album with id: ${albumId}. Album was not found.` });
                }

                // If delete images option has been set delete images from db and local storage
                if (deleteAlbumImages) {
                    // Delete files from local storage
                    var images = album.images;

                    if (images && Array.isArray(images)) {
                        images = images.map((image) => `./app/public/images/${image.filename}`);
                        deleteFiles(images, function(err) {
                            if (err) {
                                console.log(err);
                                return res.status(500).send({ message: "An unexpected error occurred." });
                            }
                        });
                    }

                    // Delete images from the database
                    Image.deleteMany({ _id: {$in: (album.images)} })
                        .exec((err, res) => {
                            if (err) {
                                console.log(err);
                                return res.status(500).send({ message: "An unexpected error occurred." });
                            }

                            return res.send({ message: "Album deleted successfully." });
                        });
                } else {
                // Other wise just set the album viewers array to an empty array
                    Image.updateMany({ album: album._id }, { album: null, viewers: [] })
                        .exec((err, images) => {
                            if (err) {
                                console.log(err);
                                return res.status(500).send({ message: "An unexpected error occured." });
                            }

                            return res.send({ message: "Album deleted successfully." });
                        });
                }
            });
    }

    /**
     * Adds a set of images to an album given their IDs. This request will only work if the user is the owner of said album.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    addImagesToAlbum(req, res) {
        const albumId = req.params.id;
        const images = req.body.images.map(mongoose.Types.ObjectId);

        Album.findOneAndUpdate({ _id: albumId, owner: req.userId }, { $addToSet: { images: images } })
            .exec((err, album) => {
                if (err) {
                    if (err instanceof mongoose.Error.CastError) {
                        return res.status(500).send({ message: "Invalid input." });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occured." });
                    }
                }

                if (!album) {
                    return res.status(404).send({ message: `Cannot update album with id: ${albumId}. Album was not found.` });
                }

                // Add album viewers to images
                Image.updateMany({ _id: {  $in: images } }, { album: album._id, viewers: album.viewers })
                    .exec((err, updateRes) => {
                        if (err) {
                            console.log(err);
                            return res.status(500).send({ message: "An unexpected error occured." });
                        }

                        return res.send({ message: `Album updated successfully. ${updateRes.nModified} images added to the album.` });
                    });
            });
    }

    /**
     * Removes a set of images to an album given their IDs. This request will only work if the user is the owner of said album.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    removeImagesFromAlbum(req, res) {
        const albumId = req.params.id;
        const images = req.body.images.map(mongoose.Types.ObjectId);

        Album.findOneAndUpdate({ _id: albumId, owner: req.userId }, { $pull: { images: { $in: images } } })
            .exec((err, album) => {
                if (err) {
                    if (err instanceof mongoose.Error.CastError) {
                        return res.status(500).send({ message: "Invalid input." });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occured." });
                    }
                }

                if (!album) {
                    return res.status(404).send({ message: `Cannot update album with id: ${albumId}. Album was not found.` });
                }

                // Remove album viewers to images
                Image.updateMany({ _id: {  $in: images } }, { album: null, viewers: [] })
                    .exec((err, updateRes) => {
                        if (err) {
                            console.log(err);
                            return res.status(500).send({ message: "An unexpected error occured." });
                        }

                        return res.send({ message: `Album updated successfully. ${updateRes.nModified} images removed to the album.` });
                    });
            });
    }

    /**
     * Gets information on a specific album given the ID.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
     getAlbum(req, res) {
        const albumId = req.params.id;

        // Check if file exists in database
        Album.findById(albumId)
            .exec((err, album) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (!album) {
                    return res.status(404).send({ message: "Album Not found." });
                }

                // Checking if user has rights to view this album
                var isOwner = album.owner == req.userId;
                var isViewer = false;
                if (Array.isArray(album.viewers)) {
                    isViewer = (album.viewers.findIndex((elem) => elem == req.userId) !== -1);
                }

                if (!isOwner && !isViewer) {
                    return res.status(403).send({ message: "Unauthorized!" });
                }

                return res.send(album);
            });
    }

    /**
     * Updates the properties of an album given it's ID. Only certain properties of album can be updated through this endpoint.
     * The `id` of an album can never be updated. Once the `owner` of an album has been assigned suring creation it can never be
     * changed. Adding `images` to an album through this route is forbidden as there is an endpoint dedicated to this task.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    update(req, res) {
        const albumId = req.params.id;
        const albumData = req.body ? req.body : {};

        // Remove any potentially dangerous update attributes
        if ('_id' in albumData) {
            return res.status(500).send({ message: "Invalid input. Cannot update album id." });
        }

        if ('images' in albumData) {
            return res.status(500).send({ message: "Invalid input. Cannot update album images. Use dedicated route." });
        }

        if ('owner' in albumData) {
            return res.status(500).send({ message: "Invalid input. Cannot update album owner." });
        }

        Album.findOneAndUpdate({ _id: albumId, owner: req.userId }, albumData, { new: true })
            .exec((err, album) => {
                if (err) {
                    if (err instanceof mongoose.Error.CastError) {
                        return res.status(500).send({ message: "Invalid input." });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occured." });
                    }
                }

                if (!album) {
                    return res.status(404).send({ message: `Cannot update album with id: ${albumId}. Album was not found.` });
                }

                res.send({ message: "Album updated successfully." });
            });
    }

    /**
     * Gets all images associated to an album that the current user has access to(being the owner or an allowed viewer).
     * Results are paginated by default to prevent long response times and heavy response bodies. The page number and
     * page size can be specified through the uses of the query parameters `page` and `size`. **Note** that the page
     * number always starts at `0`.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    getAlbumImages(req, res) {
        const albumId = req.params.id;
        const page = req.query.page ? parseInt(req.query.page) : null;
        const size = req.query.size ? parseInt(req.query.size) : null;
        const { limit, offset } = this.getPagination(page, size);

        const condition = {
            album: albumId,
            $or: [
                { owner: req.userId },
                { viewers: req.userId }
            ]
        };

        Image.paginate(condition, { offset, limit })
            .then((data) => {
                res.send({
                    totalItems: data.totalDocs,
                    totalPages: data.totalPages,
                    currentPage: data.page - 1,
                    images: data.docs,
                });
            }).catch((err) => {
                console.log(err);
                res.status(500).send({ message: "An unexpected error occured." });
            });

    }

    /**
     * Parse a JSON object into a new `Album` object
     * @param {object} albumData JSON object containing `Album` properties
     * @returns a new `Album`
     */
    parse(albumData) {
        const { title, description, viewers } = albumData;

        return new Album({
            title: title,
            description: description,
            viewers: viewers
        });
    }

    /**
     * Gets the limit and offset values for a query given page number and page size.
     * @param {number} page Page number
     * @param {number} size Page size
     * @returns an object containing the limit and offset
     */
    getPagination(page, size) {
        const maxLimit = 50;
        var limit = size ? (size > maxLimit ? maxLimit : size) : 10;
        var offset = page ? page * limit : 0;

        return { limit, offset };
    }


}

exports.albumController = new AlbumController();
const fs = require('fs');
const multer = require('multer');
const multerConfig = require('../configs/multer.config.');
const path = require('path');

const { Album, Image, mongoose } = require("../models");


class ImageController {

    /**
     * Uploads and saves images passed in a form-data request body. On creation the `album` and `viewers` paramaters are
     * ignored. The `owner` parameter is also ignored as it is deduced from the JWT token.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    upload(req, res) {
        const upload = multer({
            allowedImage: multerConfig.imageFilter,
            storage: multerConfig.imageStorage()
        }).array("images", parseInt(process.env.MAX_IMAGE_UPLOAD));

        upload(req, res, function (err) {
            if (err) {
                if (err instanceof multer.MulterError) {
                    if (err.code === "LIMIT_UNEXPECTED_FILE") { // Too many images exceeding the allowed limit
                        return res.status(400).send({ message: `Error. Maximum image upload limited to ${process.env.MAX_IMAGE_UPLOAD}.` });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occured." });
                    }
                } else {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }
            } else {
                // Create the image objects to be uploaded
                var images = [];

                if (!res.req.files) {
                    console.log(res.req);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                for (let i = 0; i < res.req.files.length; i++) {
                    var image = {
                        filename: res.req.files[i].filename,
                        description: res.req.body.description,
                        album: null,
                        viewers: [],
                        owner: req.userId,
                        tags: res.req.body.tags
                    };

                    images.push(image)
                }

                // Upload the images to the database
                if (images.length) {
                    Image.insertMany(images, (err, docs) => {
                        if (err) {
                            console.log(err);
                            return res.status(500).send({ message: "Unable to save one of the images provided." });
                        } else {
                            return res.send({ message: `${docs.length} images uploaded successfully.` });
                        }
                    });
                }
            }
        });
    }

    /**
     * Uploads and save a single image passed in a form-data request body. On creation the `album` and `viewers` paramaters are
     * ignored. The `owner` parameter is also ignored as it is deduced from the JWT token.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    uploadSingle(req, res) {
        const upload = multer({
            allowedImage: multerConfig.imageFilter,
            storage: multerConfig.imageStorage()
        }).single("image");

        upload(req, res, function (err) {
            if (err) {
                if (err instanceof multer.MulterError) {
                    if (err.code === "LIMIT_UNEXPECTED_FILE") { // Too many images exceeding the allowed limit
                        return res.status(400).send({ message: `Error. Maximum image upload limited to ${process.env.MAX_IMAGE_UPLOAD}.` });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occured." });
                    }
                } else {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }
            } else {
                const newImage = new Image({
                    filename: res.req.file.filename,
                    description: res.req.body.description,
                    owner: req.userId,
                    album: null,
                    viewers: [],
                    tags: res.req.body.tags
                });

                newImage.save((err, image) => {
                    if (err) {
                        console.log(err);
                        return res.status(500).send({ message: "Unable to save the image provided." });
                    } else {
                        return res.send({ message: "Image uploaded." });
                    }
                });
            }
        });
    }

    /**
     * Deletes an image that the current user is the owner of. This deletes the image from local storage as well
     * as from the database. If the image is part of an album, the image is removed from the album and updated
     * in the database.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    delete(req, res) {
        const imageId = req.params.id;

        Image.findOneAndDelete({ _id: imageId, owner: req.userId })
            .exec((err, image) => {
                if (err) {
                    if (err instanceof mongoose.Error.CastError) {
                        return res.status(400).send({ message: "Invalid input." });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occurred." });
                    }
                }

                if (!image) {
                    return res.status(404).send({ message: `Cannot delete image with id: ${imageId}. Image was not found.` });
                }

                // Delete file from local storage
                const filepath = `./app/public/images/${image.filename}`;
                if (fs.existsSync(filepath)) {
                    fs.unlinkSync(filepath);
                }

                if (image.album) {
                    Album.updateOne({ _id: image.album, owner: req.userId }, { $pull: { images: imageId } })
                        .exec((err, result) => {
                            if (err) {
                                if (err instanceof mongoose.Error.CastError) {
                                    return res.status(500).send({ message: "Invalid input." });
                                } else {
                                    console.log(err);
                                    return res.status(500).send({ message: "An unexpected error occured." });
                                }
                            }

                            return res.send({ message: "Image deleted successfuly." });
                        });
                } else {
                    return res.send({ message: "Image deleted successfuly." });
                }
            });
    }

    /**
     * Gets a specific image given the filename.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    viewImage(req, res) {
        const imageDir = './app/public/images/';
        const filename = req.params.filename;
        const supportedMimeTypes = {
            gif: 'image/gif',
            jpg: 'image/jpeg',
            png: 'image/png'
        };

        // Check if file exists in database
        Image.findOne({ filename: filename })
            .lean()
            .exec((err, image) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (!image) {
                    return res.status(404).send({ message: "Image Not found." });
                }

                // Checking if user has rights to view this image
                // var isViewer = false;
                // if (Array.isArray(image.viewers)) {
                //     isViewer = (image.viewers.findIndex((elem) => elem === req.userId) !== -1);
                // }

                // console.log(req.userId);
                // if ((image.owner != req.userId) && !isViewer) {
                //     return res.status(403).send({ message: "Unauthorized!" });
                // }

                // Serving the image
                const file = path.join(imageDir, filename);
                const mimeType = supportedMimeTypes[path.extname(filename).slice(1)] || 'text/plain';
                const stream = fs.createReadStream(file);

                stream.on('open', function () {
                    res.set('Content-Type', mimeType);
                    stream.pipe(res);
                });

                // Cannot find image in local storage
                stream.on('error', function () {
                    return res.status(404).send({ message: 'Image not found.' });
                });
            });
    }

    /**
     * Gets information on a specific image given the ID.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
     getImage(req, res) {
            const imageId = req.params.id;

        // Check if file exists in database
        Image.findById(imageId)
            .exec((err, image) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (!image) {
                    return res.status(404).send({ message: "Image Not found." });
                }

                // Checking if user has rights to view this image
                var isOwner = image.owner == req.userId;
                var isViewer = false;
                if (Array.isArray(image.viewers)) {
                    isViewer = (image.viewers.findIndex((elem) => elem == req.userId) !== -1);
                }

                if (!isOwner && !isViewer) {
                    return res.status(403).send({ message: "Unauthorized!" });
                }

                return res.send(image);
            });
    }

    /**
     * Gets all images that the current user has access to(being the owner or an allowed viewer).
     * Results are paginated by default to prevent long response times and heavy response bodies. The page number and
     * page size can be specified through the uses of the query parameters `page` and `size`. **Note** that the page
     * number always starts at `0`.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    getImages(req, res) {
        const page = parseInt(req.query.page);
        const size = parseInt(req.query.size);
        const { limit, offset } = this.getPagination(page, size);

        const condition = {
            $or: [
                { owner: req.userId },
                { viewers: req.userId }
            ]
        };

        Image.paginate(condition, { offset, limit })
            .then((data) => {
                return res.send({
                    totalItems: data.totalDocs,
                    totalPages: data.totalPages,
                    currentPage: data.page - 1,
                    images: data.docs,
                });
            }).catch((err) => {
                console.log(err);
                return res.status(500).send({ message: "An unexpected error occured." });
            });
    }

    /**
     * Updates the properties of an image given it's ID. Only certain properties of an image can be updated through this endpoint.
     * The `id` of an image can never be updated. Once the `owner` of an image has been assigned during creation it can never be
     * changed. The filename is always set on the backend and not the terminal.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    update(req, res) {
        const imageId = req.params.id;
        var imageData = req.body ? req.body : {};

        // Remove any potentially dangerous update attributes
        if ('_id' in imageData) {
            return res.status(500).send({ message: "Invalid input. Cannot update image id." });
        }

        if ('filename' in imageData) {
            return res.status(500).send({ message: "Invalid input. Cannot update image filename." });
        }

        if ('owner' in imageData) {
            return res.status(500).send({ message: "Invalid input. Cannot update image owner." });
        }

        Image.findById(imageId)
            .exec((err, image) => {
                if (err) {
                    if (err instanceof mongoose.Error.CastError) {
                        return res.status(500).send({ message: "Invalid input." });
                    } else {
                        console.log(err);
                        return res.status(500).send({ message: "An unexpected error occured." });
                    }
                }

                if (!image) {
                    return res.status(404).send({ message: `Cannot update image with id: ${imageId}. Image was not found.` });
                }

                if (image.owner != req.userId) {
                    return res.status(403).send({ message: "Unauthorized." });
                }

                if (image.album && imageData.viewers) {
                    return res.status(400).send({ message: "Cannot update image viewers. Images is part of album." });
                }

                Image.findByIdAndUpdate(imageId, imageData, { new: true })
                    .exec((err, image) => {
                        if (err) {
                            if (err instanceof mongoose.Error.CastError) {
                                return res.status(500).send({ message: "Invalid input." });
                            } else {
                                console.log(err);
                                return res.status(500).send({ message: "An unexpected error occured." });
                            }
                        }

                        if (!image) {
                            return res.status(404).send({ message: `Cannot update image with id: ${imageId}. Image was not found.` });
                        }

                        return res.send({ message: "Image updated successfuly." });
                    });

            });
    }

    /**
     * Gets the limit and offset values for a query given page number and page size.
     * @param {number} page Page number
     * @param {number} size Page size
     * @returns an object containing the limit and offset
     */
    getPagination(page, size) {
        const limit = size ? size : 25;
        const offset = page ? page * limit : 0;

        return { limit, offset };
    }

}

exports.imageController = new ImageController();
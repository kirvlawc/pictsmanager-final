const { albumController } = require('./album.controller');
const { authController } = require('./auth.controller');
const { imageController } = require('./image.controller');

module.exports = {
    albumController,
    authController,
    imageController
}

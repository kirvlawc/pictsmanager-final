const express = require('express');
const { authMiddleware } = require('../middlewares');
const { albumController } = require('../controllers');
const router = express.Router();

router.get('/', [authMiddleware.verifyToken], albumController.getAlbums.bind(albumController));

router.post('/', [authMiddleware.verifyToken], albumController.create.bind(albumController));

router.get('/:id', [authMiddleware.verifyToken], albumController.getAlbum.bind(albumController));

router.patch('/:id', [authMiddleware.verifyToken], albumController.update.bind(albumController));

router.delete('/:id', [authMiddleware.verifyToken], albumController.delete.bind(albumController));

router.get('/:id/images', [authMiddleware.verifyToken], albumController.getAlbumImages.bind(albumController));

router.post('/:id/images', [authMiddleware.verifyToken], albumController.addImagesToAlbum.bind(albumController));

router.post('/:id/images/delete', [authMiddleware.verifyToken], albumController.removeImagesFromAlbum.bind(albumController));

exports.albumRoutes = router;
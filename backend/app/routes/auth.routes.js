const express = require('express');
const { authController } = require('../controllers');
const { authMiddleware } = require('../middlewares');
const router = express.Router();

router.post('/verifyemail', authController.emailExists.bind(authController));

router.post('/verifyusername', authController.usernameExists.bind(authController));

router.post(
    '/signup',
    [
        authMiddleware.checkDuplicateUser,
        authMiddleware.checkRolesExist
    ],
    authController.signup.bind(authController)
);

router.post('/signin', authController.signin.bind(authController));

router.post("/refreshtoken", authController.refreshToken.bind(authController));

exports.authRoutes = router;
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2')

var albumSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true
        },
        description: String,
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
        },
        images: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Image"
        }],
        viewers: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }]
    },
    {
        timestamps: {
            createdAt: "created_at",
            updatedAt: "updated_at"
        }
    }
);

albumSchema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;

    return object;
});

albumSchema.plugin(mongoosePaginate);

const Album = mongoose.model(
    "Album",
    albumSchema
);

exports.Album = Album;
const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2')

var imageSchema = new mongoose.Schema(
    {
        filename: {
            type: String,
            required: true
        },
        description: String,
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
        },
        album: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Album"
        },
        viewers: {
            type: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            }]
        },
        tags: [String]
    },
    {
        timestamps: {
            createdAt: "created_at",
            updatedAt: "updated_at"
        }
    }
);

imageSchema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    object.url = `${process.env.BASE_URL}/api/images/view/${object.filename}`;

    return object;
});

imageSchema.plugin(mongoosePaginate);

const Image = mongoose.model(
    "Image",
    imageSchema
);

exports.Image = Image;
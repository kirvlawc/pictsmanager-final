const authConfig = require("../configs/auth.config");
const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');

var refreshTokenSchema = new mongoose.Schema(
    {
        token: {
            type: String,
            required: true
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
        },
        expiry_date: {
            type: Date,
            required: true
        },
    },
    {
        timestamps: {
            createdAt: "created_at",
            updatedAt: "updated_at"
        }
    }
);

refreshTokenSchema.statics.createToken = async function (user) {
    let expiredAt = new Date();

    expiredAt.setSeconds(expiredAt.getSeconds() + authConfig.jwtRefreshExpiration);

    let _token = uuidv4();

    let _object = new this({
        token: _token,
        user: user._id,
        expiry_date: expiredAt.getTime(),
    });

    let refreshToken = await _object.save();

    return refreshToken.token;
};

refreshTokenSchema.statics.verifyExpiration = (token) => {
    return token.expiry_date.getTime() < new Date().getTime();
}

refreshTokenSchema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;

    return object;
});

const RefreshToken = mongoose.model(
    "RefreshToken",
    refreshTokenSchema
);

exports.RefreshToken = RefreshToken;
const mongoose = require('mongoose');
const { Album } = require('./album.model');
const { Image } = require('./image.model');
const { RefreshToken } = require('./refresh-token.model');
const { Role } = require('./role.model');
const { User } = require('./user.model');

mongoose.Promise = global.Promise;

const db = {
    Album: Album,
    Image: Image,
    mongoose: mongoose,
    Role: Role,
    RefreshToken: RefreshToken,
    ROLES: JSON.parse(process.env.ROLES),
    User: User
};

module.exports = db;